
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ArController : MonoBehaviour
{
  
    GameObject btTakePic; 

    private void Awake()
    {
        btTakePic = GameObject.Find(Constantes.BTN_TAKE_PICTURE);
    }
    private void Start()
    {

    }

    private void LateUpdate()
    {
  
    }

    public void TakePicture()
    {
        Debug.Log("Btn Share Clicked");


        if (!System.IO.Directory.Exists("screenshots"))
        {
            System.IO.Directory.CreateDirectory("screenshots");
        }
        // masquer le boutton avant capture 
        btTakePic.SetActive(false);

        ScreenCapture.CaptureScreenshot("screenshots/maCapture.png"); //ecrase le fichier s'il existe
        Debug.Log("capture ecran enregistr�e");

        //ajouter son ou element Ui pour que l'user soit inform� de la capture

        SceneManager.LoadScene(Constantes.FORM_SCREEN);
    }


    public void Restart()
    {
        //SceneManager.LoadScene("AR_coloring_page");
         SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


}
