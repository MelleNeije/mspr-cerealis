
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Collections;

public class FormController : MonoBehaviour
{
    public GameObject[] fieldGroupArr;
    public FormDataClass[] qaArr;

    public Text messageText;
    public InputField emailInput;
    public InputField nameInput;

    void Start()
    {
        emailInput = (InputField)GetComponent("EmailField");
        nameInput = (InputField)GetComponent("name");

        qaArr = new FormDataClass[2]; //cr�ation d'un array pour y stocker les donn�es

    }


    public void Submit()
    {

        SauvegarderDatas(); 

        StartCoroutine(LaunchShare());

    }

    private void SauvegarderDatas()
    {
        DataEmail();
        DataName(); 

    }

    private void  DataEmail()
    {
        FormDataClass datas = new FormDataClass();
        datas.field = Constantes.EMAIL_FIELD;
        if (emailInput.text != "")
        {
            datas.value = emailInput.text;
        }
        qaArr[1] = datas;
    }
    private void DataName()
    {
        FormDataClass datas = new FormDataClass();
        datas.field = Constantes.NAME_FIELD;
        if (nameInput.text != "")
        {
            datas.value = nameInput.text;
        }
        qaArr[0] = datas;
    }


    private IEnumerator LaunchShare()
    {
        yield return new WaitForEndOfFrame();

        string filePath =Constantes.FILE_CAPTURE_PATH;
            Debug.Log("before native Share"); 

        new NativeShare().AddFile(filePath)
            .SetText("Here is my painting!").SetUrl(Constantes.NATIVE_SHARE_URL)
            .SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
            .Share();

    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

}

