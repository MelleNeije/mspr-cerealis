

public class Constantes
{
    //ECRANS 
    public static string AR_SCREEN = "AR_coloring_page";
    public static string FORM_SCREEN = "Form";


    //BOUTTONS
    public static string BTN_SHARE = "AR_coloring_page";
    public static string BTN_TAKE_PICTURE = "TakePictureBtn";

    
    public static string NAME_FIELD = "name";
    public static string EMAIL_FIELD = "TextField";

    //FICHIERS
    public static string FILE_CAPTURE_PATH = "screenshots/maCapture.png";
    // URI
    public static string NATIVE_SHARE_URL = "https://github.com/yasirkula/UnityNativeShare";
}

